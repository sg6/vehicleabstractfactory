/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicleabstractfactory.vehicles;

/**
 *
 * @author stefan
 */
public abstract class Vehicle {
    
    private String brand;
    private int tires;
    private String name;
    
    public Vehicle(String b, int t, String n) {
        brand = b;
        tires = t;
        name  = n;
        createVehicle();
    }

    /**
     * @return the brand
     */
    public String getBrand() {
        return brand;
    }

    /**
     * @param brand the brand to set
     */
    public void setBrand(String brand) {
        this.brand = brand;
    }

    /**
     * @return the tires
     */
    public int getTires() {
        return tires;
    }

    /**
     * @param tires the tires to set
     */
    public void setTires(int tires) {
        this.tires = tires;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }
    
    public void createVehicle() {
        System.out.println("Vehicle " + name + " created! Brand: " + brand + ","+ tires + " tires\n");
    }
    
    
}
