/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicleabstractfactory.vehicles;

/**
 *
 * @author stefan
 */
public class Car extends Vehicle {

    public Car(String brand) {
        super(brand, 4, "Car");
    }
   
}
