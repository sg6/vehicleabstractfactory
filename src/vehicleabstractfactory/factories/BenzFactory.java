/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicleabstractfactory.factories;
import vehicleabstractfactory.vehicles.*;

/**
 *
 * @author stefan
 */
public class BenzFactory {
    public static final int CAR = 0;
    public static final String BRAND = "Mercedes Benz";

    public static Vehicle createVehicle(int i){
        System.out.println("Benz in production...");
        switch(i){
        case CAR:
            return new Car(BRAND);
        default:
            throw new IllegalArgumentException("Wrong vehicle number!");
        }
    } 
}
