/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicleabstractfactory.factories;
import vehicleabstractfactory.vehicles.*;

/**
 *
 * @author stefan
 */
public class VehicleFactory {
    
    public static Vehicle buildVehicle(String vehicleType, String brand) {
        Vehicle v = null;
        
        switch(vehicleType.toLowerCase()) {
            case "car" :
                switch(brand.toLowerCase()) {
                    case "honda" :
                        v = HondaFactory.createVehicle(HondaFactory.CAR);
                        break;
                    case "mercedes benz" :
                        v = BenzFactory.createVehicle(BenzFactory.CAR);
                        break;
                    default :
                        System.out.println("No such car " + brand);
                        return null;
                }
                break;
            case "motorcycle" :
                if(brand == "Honda") {
                    v = HondaFactory.createVehicle(HondaFactory.MOTORCYCLE);
                }
                else {
                    System.out.println("No such motorcycle: " + brand.toLowerCase());
                    return null;
                }
                break;
            default :
                System.out.println("No such vehicle '" + vehicleType + "', "
                        + "please enter Car or Motorcycle");
                break;
        }
        
        return v;
    }
    
}
