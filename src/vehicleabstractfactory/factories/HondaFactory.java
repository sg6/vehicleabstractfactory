/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicleabstractfactory.factories;
import vehicleabstractfactory.vehicles.*;

/**
 *
 * @author stefan
 */
class HondaFactory{
    public static final int CAR = 0;
    public static final int MOTORCYCLE = 1;
    
    public static final String BRAND = "HONDA";

    public static Vehicle createVehicle(int i){
        System.out.println("Your Honda will be ready soon!");
        switch(i){
        case CAR:
            return new Car(BRAND);
        case MOTORCYCLE:
            return new Motorcycle(BRAND);
        default:
            throw new IllegalArgumentException("Wrong vehicle number!");
        }
    }
}
