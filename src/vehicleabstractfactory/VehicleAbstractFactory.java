/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehicleabstractfactory;

import vehicleabstractfactory.factories.VehicleFactory;

/**
 *
 * @author stefan
 */
public class VehicleAbstractFactory {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.print("\n\n\n");
        
        VehicleFactory.buildVehicle("Car", "Honda");
        VehicleFactory.buildVehicle("Car", "mErcedes benZ");
        VehicleFactory.buildVehicle("MotorcyCLE", "Honda");
        VehicleFactory.buildVehicle("Motorcycle", "sdfk");
        VehicleFactory.buildVehicle("CAR", "Audi");
        VehicleFactory.buildVehicle("Traktor", "XYZ");
        
        
        System.out.print("\n\n\n");
    }
    
}
